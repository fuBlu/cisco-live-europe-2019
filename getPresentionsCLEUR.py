__author__ = 'Simon Melotte'

import os
import requests
import sys
import json
import re

def createDirectory(directory):
    try: 
        if not os.path.exists(directory):
            os.mkdir(directory)
    except OSError:  
        print ("Creation of the directory %s failed" % directory)
    else:  
        print ("Successfully created the directory %s " % directory)

def getPresentations(url, filename, directory):
    with open(filename) as json_file:  
        data = json.load(json_file)
        rex = re.compile(r'\W+')
        
        for item in data: 
            print('Start downloading: ' + item['name'] + ' - description = ' + item['description'])            
            filename = item['name'].rstrip() + '.pdf'
            long_filename = rex.sub(' ', item['description'] ) + '.pdf'
            fullpath = url + filename
            print ('fullpath is: ' + fullpath)
            try:
                r = requests.get(fullpath)
                if r.status_code == 200:
                    print ('File was found: ' + filename)
                    with open( os.path.join(directory, long_filename), 'wb') as f:  
                        f.write(r.content)
                else:
                    print ('File was not found: ' + filename)
            except:
                print ("Exception on " + filename)

def main():    
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    PDFS =  os.path.join(THIS_FOLDER, 'pdfs')
    URL = 'https://clnv.s3.amazonaws.com/2019/eur/pdf/'

    filename = THIS_FOLDER + os.sep + 'cleur-sessions.json'
    createDirectory(PDFS)
    getPresentations(URL, filename, PDFS)

if __name__ == "__main__":
    main()